<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <?php #TODO: Fix dynamic page title 
  ?>
    <meta charset="utf-8">
    <title>
      <?php
            
            global $results;
            echo htmlspecialchars( $results["pageTitle"] )
            ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/simple/css/default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/simple/css/layout.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/simple/css/media-queries.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/simple/css/custom.css">
    <script src="<?php echo base_url(); ?>themes/simple/js/modernizr.js"></script>
    <link rel="shortcut icon" href="favicon.png">
</head>

<body>
  <header id="top">
    <div class="row">
      <div class="header-content twelve columns">
        <h1 id="logo-text"><a href="<?php echo base_url(); ?>" title="">Kunle's Blog</a></h1>
        <p id="intro">Olakunle Oladimeji's personal blog and website</p>
      </div>
    </div>
    <nav id="nav-wrap">
      <a href="#nav-wrap" class="mobile-btn" title="Show Navigation">Show Menu</a>
      <a href="#" class="mobile-btn" title="Hide navigation">Hide Menu</a>
      <div class="row">
        <ul id="nav" class="nav">
          <li><a href="/home/">Home</a></li>
          <li class="has-children">
            <a href="/projects/">IT Projects</a>
            <ul>
              <li><a href="/projects/1/">Project 01</a></li>
              <li><a href="/projects/2/">Project 02</a></li>
              <li><a href="/projects/3/">Project 03</a></li>
            </ul>
          </li>
          <li class="current"><a href="/photos/">Photography</a></li>
          <!--<li><a href="archives.html">About Me</a></li>-->
          <li class="has-children">
            <a href="/blogs/">Blog</a>
            <ul>
              <!-- Later, I want this part to drop down to add a new blog if I am logged in as admin -->
              <li><a href="/blogs/">Blog Entries</a></li>
              <li><a href="/blogs/new/">New Blog</a></li>
            </ul>
          </li>
          <li><a href="/contact/">Contact</a></li>
        </ul>
        </li>
        </ul>
      </div>
    </nav>
  </header>
  <?php //echo $_SERVER['REQUEST_URI']. "<br />" . base_url(); ?>