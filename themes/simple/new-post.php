<div id="content-wrap">
  <div class="row add-bottom">
    <div class="ten columns add-bottom centered">
      <?php if (isset($result)) { ?>
        <h3><?php echo $result ?></h3>
        <?php } ?>
          <h3 class="add-bottom">New Post</h3>
          <form action="" method="post">
            <fieldset>
              <label for="title">Article Title</label>
              <input type="text" name="title" id="title">
              <label for="date">Date</label>
              <input type="date" name="date" id="date" min="<?php echo date('Y-m-d'); ?>">
              <input type="hidden" name="author" id="author" value="Olakunle Oladimeji">
              <label for="message">Post Content</label>
              <textarea name="content" id="content" rows="7"></textarea>
              <button type="submit">Submit Form</button>
            </fieldset>
          </form>
    </div>
    <div class="four columns add-bottom">
      <img src="" alt="">
      <br>
      <p id="quote"></p>
      <p id="details"></p>
    </div>
  </div>
</div>