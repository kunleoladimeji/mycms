<footer>
  <div class="row">
    <div class="twelve columns">
      <ul class="social-links">
        <li><a href="/facebook/" target="_blank"><i class="fa fa-facebook"></i></a></li>
        <li><a href="/twitter/" target="_blank"><i class="fa fa-twitter"></i></a></li>
        <li><a href="/gplus/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="/bitbucket/" target="_blank"><i class="fa fa-bitbucket"></i></a></li>
        <li><a href="/skype/" target="_blank"><i class="fa fa-skype"></i></a></li>
      </ul>
    </div>
    <div class="six columns info">
      <h3>About Keep It Simple</h3>
      <p>Eveniet praesentium, facilisi recusandae, bibendum morbi, minim, quasi? Accusamus! Litora mattis nisl, molestias similique, deleniti semper eius doloribus rhoncus, sagittis.</p>
      <p>Congue delectus, sequi venenatis torquent vitae, itaque convallis leo, eum.</p>
    </div>
    <div class="four columns">
      <h3>Photostream</h3>
      <ul class="photostream group">
        <li><a href=""><img src="<?php echo base_url(); ?>themes/simple/images/thumb.jpg" alt="thumbnail"></a></li>
        <li><a href=""><img src="<?php echo base_url(); ?>themes/simple/images/thumb.jpg" alt="thumbnail"></a></li>
        <li><a href=""><img src="<?php echo base_url(); ?>themes/simple/images/thumb.jpg" alt="thumbnail"></a></li>
        <li><a href=""><img src="<?php echo base_url(); ?>themes/simple/images/thumb.jpg" alt="thumbnail"></a></li>
        <li><a href=""><img src="<?php echo base_url(); ?>themes/simple/images/thumb.jpg" alt="thumbnail"></a></li>
      </ul>
    </div>
    <div class="two columns">
      <h3 class="social">Navigate</h3>
      <ul class="navigate group">
        <li><a href="<?php //echo $routes->generate('home'); ?>">Home</a></li>
        <li><a href="">IT Projects</a></li>
        <li><a href="">Photography</a></li>
        <li><a href="">Blog</a></li>
        <li><a href="">Contact</a></li>
      </ul>
    </div>
    <p class="copyright">Theme: <a href="http://www.styleshout.com/">Keep it Simple</a></p>

  </div>
  <div id="go-top"><a href="#top" class="smoothscroll" title="Back to Top"><i class="fa fa-chevron-up"></i></a></div>
</footer>

<script src="<?php echo base_url(); ?>themes/simple/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>themes/simple/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>themes/simple/js/main.js"></script>
</body>

</html>