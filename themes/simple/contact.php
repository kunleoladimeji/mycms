<div id="content-wrap">
<div class="row add-bottom">
    <div class="ten columns add-bottom centered">
        <h3 class="add-bottom">Get in Touch</h3>
        <form action="" method="post">
            <fieldset>
                <label for="name">Name</label>
                <input type="text" name="" id="name">
                <label for="email">Email</label>
                <input type="text" name="" id="email">
                <label for="message">Message</label>
                <input type="text" name="" id="message">
                <label for="captcha">Captcha</label>
                <div id="captcha"></div>
                <button type="submit">Submit Form</button>
            </fieldset>
        </form>
    </div>
    <div class="four columns add-bottom">
        <img src="" alt="">
        <br>
        <p id="quote"></p>
        <p id="details"></p>
    </div>
</div>
</div>