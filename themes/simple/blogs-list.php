<div id="content-wrap">
    <div class="row">
        <div id="main" class="eight columns">
        <?php echo json_encode($results['articles']) ?>
        <?php foreach ($results['articles'] as $article) { ?>
            <article class="entry">
                <header class="entry-header">
                    <h2 class="entry-title"><a href="<?php echo $article->the_permalink(); ?>"><?php echo $article->the_title(); ?></a></h2>
                    <div class="entry-meta">
                        <ul>
                            <li><?php echo $article->publication_date(); ?></li>
                            <span class="meta-sep">•</span>
                            <li><a href="#"><?php echo $article->the_categories(); #or make it the_tag()?></a></li>
                            <span class="meta-sep">•</span>
                            <li><?php echo $article->the_author(); ?></li>
                        </ul>
                    </div>
                </header>
                <div class="entry-content">
                    <p><?php echo $article->the_content(); ?></p>
                </div>
            </article>
        <?php } ?>
            <div class="pagenav">
                <p>
                    <a href="#" rel="prev">Prev</a>
                    <a href="#" rel="next">Next</a>
                </p>
            </div>
        </div>
        <div id="sidebar" class="four columns">
            <div class="widget widget_search">
                <h3>Search</h3>
                <form action="/blogs/search/" method="get">
                    <input type="text" name="s" id="s" class="text-search" placeholder="Search here...">
                    <input type="submit" value="" class="submit-search">
                </form>
            </div>
            <div class="widget widget_categories group">
                <h3>Categories</h3>
                <ul>
                    <?php foreach (Article::getList('categories') as $title => $count) { ?>
                        <li><a href="/blogs/tag/<?php echo $title; ?>" title><?php echo $title ?></a> " (<?php echo $count; ?>)"</li>
                    <?php } ?>
                </ul>
            </div>
            <div class="widget widget_tags">
                <h3>Post Tags.</h3>
                <div class="tagcloud group">
                    <?php foreach (Article::getList('tags') as $tag) {?>
                        <a href="/blogs/category<?php echo $title; ?>"><?php echo $title; ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="widget widget_popular">
                <h3>Popular Posts</h3>
                <ul class="link-list">
                    <?php foreach (Article::getList('popular') as $results) { ?>
                        <li><a href="/blogs/post/<?php echo $results['id']?>"><?php echo $results['title'] ?></a>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>