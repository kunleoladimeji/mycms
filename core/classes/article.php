<?php
/**
* Class to handle articles
*/
class Article
{
    #This will later be turned into a private variable soon
    private $_db;
    public $id;
    private $author;
    private $publicationDate;
    private $title;
    private $summary;
    private $content;
    private $category;
    private $tag;
    public $results=array();
    public $error;
    
    public function __construct($data = array())
    {
        $this->_db = Database::getInstance();
        if (isset($data['id'])) {
            $this->id = (int) $data['id'];
        }
        if (isset($data['author'])) {
            $this->author = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['author']);
        }
        if (isset($data['publicationDate'])) {
            $this->publicationDate = (int) $data['publicationDate'];
        }
        if (isset($data['title'])) {
            $this->title = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['title']);
        }
        if (isset($data['summary'])) {
            $this->summary = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['summary']);
        }
        if (isset($data['content'])) {
            $this->content = $data['content'];
        }
        if (isset($data['category'])) {
            $this->content = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['category']);
        }
        if (isset($data['tag'])) {
            $this->content = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['tag']);
        }
        #Temporary holder for now. Will sort otu later
        if (!empty($data)) {
            $this->results = $data;
        }
    }
    
    public function storeFormValues($params)
    {
        $this->__construct($params);
        // if (isset($params['publicationDate'])) {
        //     $publicationDate = explode('-', $params['publicationDate']);
        //     if (count($publicationDate) == 3) {
        //         list($y, $m, $d) = $publicationDate;
        //         $this->publicationDate = mktime(0, 0, 0, $m, $d, $y);
        //     }
        // }
    }
    
    public static function getById($id)
    {
        if (!is_null($id)) {
            $data = $this->_db->get('blog', array($field, '=', $id));
            if ($data->count()) {
                return new Article($data);
            }
        }
    }
    
    #static func?
    public function getList($type, $numRows=1000000, $order='publicationDate DESC')
    {
        switch ($type) {
            case 'articles':
                #Dont forget to expand Database->get() class to allow for ORDER BY AND LIMIT AND WHERE commands
                if ($this->_db->get('articles')) {
                    $list = array();
                    #get json dump of results
                    for ($i=0; $i < sizeof($this->_db->results()); $i++) {
                        $article = new Article($this->_db->_results[i]);
                        $list[] = $article;
                    }
                    return (array('results' => $list, 'totalRows' => $this->_db->count()));
                }
                break;
            case 'tags':
                # code...
                break;
            case 'categories':
                # code...
                break;
            case 'popular':
                # code...
                break;
            default:
                # code...
                break;
        }
    }

    //static or nah?
    public function insert()
    {
        if (!is_null($this->id)) {
            trigger_error('Article::insert(): Article already has an ID already set', E_USER_ERROR);
        }
        $articleArray = array(
        
        'publicationDate' => $this->publicationDate,
        'title' => $this->title,
        'summary' => $this->summary,
        'content' =>$this->content
        );
        if ($this->_db->insert('articles', $articleArray)) {
            $this->id = $this->_db->_insertID;
            return true;
        } else {
            $this->error = "Article::insert() Error inserting into Database";
            return false;
        }
    }

    public function update()
    {
        if (is_null($this->id)) {
            trigger_error('Article::update(): Attempt to update an Article object with no ID property set', E_USER_ERROR);
        }
        $articleArray = array(
    'publicationDate' => $this->publicationDate,
    'title' => $this->title,
    'summary' => $this->summary,
    'content' => $this->content,
    'id' => $this->id );
        $this->_db->update('blog', $articleArray, $this->id);
    }

    public function delete()
    {
        if (is_null($this->id)) {
            trigger_error('Article::delete: Attempt to delete an Article object with no ID property set', E_USER_ERROR);
        }
        $this->_db->delete('blog', array('id', '=', $this->id));
    }

    public function trim_content($input, $length = 50, $ellipses = true, $strip_html = true)
    {
        if (is_null($input)) {
            trigger_error('trim_content(): Attempt to trim null content', E_USER_ERROR);
        }
    
        if ($strip_html) {
            $input = strip_tags($input);
        }
    
    #Less than 50 characters, no need to trim
    if (mb_strlen($input) < $length) {
        return $input;
    }
    
        $last_space = mb_strpos(substr($input, 0, $length), ' ');
        $trimmed_text = ($last_space === false) ? mb_substr($input, 0, $length, 'UTF-8') : mb_substr($input, 0, $last_space, 'UTF-8');
    
    #add ellipses '...'
    if ($ellipses) {
        $trimmed_text .= '...';
    }
    
        return $trimmed_text;
    }

#Exposes some functions for use without calling instance of Article class
    public static function article_functions()
    {
        if (!empty(self::$results)) {
            function the_content()
            {
                return self::$results["content"];
            }
            function publication_date()
            {
                return self::$results["publication_date"];
            }
            function the_author()
            {
                return self::$results["author"];
            }
            function the_title()
            {
                return self::$results["title"];
            }
            function the_categories()
            {
                $cats_list = "";
                $cats = explode('|', $results['categories']);
                foreach ($cats as $cat) {
                    $cats_list .= "<a href='/blogs/tag/".$cat. "'>".$cat."</a>";
                }
                return $cats_list;
            }
        } else {
            trigger_error('Error here', E_USER_ERROR);
        }
    }
}