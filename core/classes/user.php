<?php

/**
* User Authentication Class
*
* TODO: Check for permissions
*/
class User
{
	private $_db, $_data, $_sessionName, $_cookieName, $isLoggedIn, $errorMsg, $statusMsg;

	public function __construct($user=null)
	{
		$this->_db = Database::getInstance();
		if(!$user) {
			if(Session::exists($this->_sessionName)) {
				$user = Session::get($this->_sessionName);
				if($this->find($user)) {
					$this->_isLoggedIn = true;
				}
				else {
					#User is logged out
				}
			}
		}
		else {
			$this->find($user);
		}
	}

	public function login($username = null, $password= null, $remember = false)
	{
		if (!$username && !$password && $this->data()) {
			Session::put($this->_sessionName, $this->data()->id);
		}
		else {
			$user = $this->find($username);
			if($user)
			{
				if($this->data()->password === Hash::make($password, $this->data()->salt)) {
					Session::put($this->_sessionName, $this->_data->id);
					if($remember) {
						$hash = Hash::unique();
						$hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->id));
						if(!is_array($hashCheck)) {
							$this->_db->insert('user_sessions', array('user_id' => $this->data()->id, 'hash' => $hash));
						}
						else {
							$hash = $hashCheck['user_sessions']['hash'];
						}
						Cookie::put($this->_cookieName, $hash, 3600);
					}
					return true;
				}
			}
		}
		return false;
	}

	public function create($fields=array())
	{
		if(!$this->_db->insert('users', $fields))
		{
			throw new Exception('Sorry, user could not be created');
		}
	}

	public function find($user=null)
	{
		if($user) {
			$field = (is_numeric($user)) ? 'id' : 'username';
			$data = $this->_db->get('users', array($field, '=', $user));
			if ($data->count()) {
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}

	public function logout()
	{
		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	}

	public function data()
	{
		return $this->_data;
	}

	public function exists()
	{
		return (!empty($this->_data)) ? true : false;
	}

	public function isLoggedIn()
	{
		return (!empty($this->_isLoggedIn)) ? true : false;
	}

	public function getPermissions()
	{
		$data = $this->data();
		return $data['group'];
	}
}

?>