<?php
/**
* Database Class
*/
class Database
{
    private static $_instance = null;
    public $_insertID;
    private $_pdo;
    private $_query;
    private $_error = false;
    private $_results;
    private $_count = 0;
    public function __construct()
    {
        try {
            #Change db_type paramter
                $this->_pdo = new PDO(Config::get('mysql/db_type'). ':host='.Config::get('mysql/host'). ';dbname=' .Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
            $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            if ($e->getCode() == 1049) {
                #Run DB sql script
                    try {
                        $path_to_sql = realpath(getcwd(). '/core/db.sql');
                        $command = 'mysql -h '.Config::get('mysql/host').' -u '.Config::get('mysql/username');
                        $command .= ' < "' .$path_to_sql. '"';
                        echo "<br />".$command; # To remove later
                        shell_exec($command) or die("Could not set up database");
                        self::getInstance();
                    } catch (Exception $e) {
                        trigger_error($e->errorMessage());
                    }
            } elseif ($e->getCode() == 1045) {
                trigger_error('<b>Incorrect DB configuration. Please check the connection parameters!</b>', E_USER_NOTICE);
            } else {
                self::closeInstance();
                trigger_error($e->getMessage(), E_USER_ERROR);
            }
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new Database();
        }
        return self::$_instance;
    }

    public static function closeInstance()
    {
        self::$_instance = null;
    }

    public function query($sql, $params=array())
    {
        $this->_error = false;
        if ($this->_query = $this->_pdo->prepare($sql)) {
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }
            if ($this->_query->execute()) {
                //$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                //$this->_insertID = $this->_query->lastInsertId();
                $this->_insertID = $this->_pdo->lastInsertId();
                $this->_count = $this->_query->rowCount();
            } else {
                $this->_error = true;
            }
        }
        return $this;
    }

    private function action($action, $table, $where=array())
    {
        #This silly line will be changed, in cases of AND statements
        if (count($where) === 3) {
            $operators = array('=', '>', '<', '>=', '<=');
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];

            if (in_array($operator, $operators)) {
                $sql = "{$action} FROM  {$table} WHERE  {$field} {$operator} ? ";
                if (!$this->query($sql, array($value))->error()) {
                    return $this;
                }
            }
        }
    }

    public function insert($table, $fields = array())
    {
        $keys = array_keys($fields);
        $values = null;
        $x = 1;
        foreach ($fields as $field) {
            $values .= '?';
            if ($x < count($fields)) {
                $values .= ', ';
            }
            $x++;
        }
        $sql = "INSERT INTO {$table} (`" .implode('`, `', $keys). "`) VALUES ({$values})";
        if (!$this->query($sql, $fields)->error()) {
            return true;
        }
        return false;
    }


    public function delete($table, $where)
    {
        if ($where) {
            return $this->action('DELETE ', $table, $where);
        } else {
            return $this->action('DROP TABLE ', $table);
        }
    }

    public function update($table, $id, $fields)
    {
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

        if (!$this->query($sql, $fields)->error()) {
            return true;
        }

        return false;
    }

    public function get($table, $where=null)
    {
        return $this->action('SELECT *', $table, $where);
    }

    public function results()
    {
        return $this->_results;
    }

    public function first()
    {
        $data = $this->results();
        return $data[0];
    }

    public function count()
    {
        return $this->_count;
    }

    public function error()
    {
        return $this->_error;
    }
}
