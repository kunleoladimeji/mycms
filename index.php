<?php
#Fix for database setup will be IP address/setuplink (e.g. 127.0.0.1/setup.php)
$results = array('pageTitle' => 'Home Page');
//$results['pageTitle'] = "Home Page";
require 'config.php';

site_header();

$match = $routes->match();
echo json_encode($match);
echo $results["pageTitle"];
if ($match && is_callable($match['target'])) {
    call_user_func_array($match['target'], $match['params']);
} else {
    include 'themes/simple/404.php';
}

site_footer();
