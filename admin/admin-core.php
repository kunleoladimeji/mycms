<?php
require(); #config.php
session_start();
$action = Input::get('action');
$username = Session::get('username');
if ($action != "login" && $action != "logout" && !$username) {
    login();
    exit;
}

switch ($action) {
    case 'login':
        login();
        break;
    case 'logout':
        logout();
        break;
    case 'newArticle':
        newArticle();
        break;
    case 'editArticle':
        editArticle();
        break;
    case 'deleteArticle':
        deleteArticle();
        break;
    case 'listArticles':
        listArticles();
        break;
    default:
        dashboard();
        break;
}

function login() {
    $results = array();
    $results['pageTitle'] = "Admin Login | MyCMS"; #do config of site name here;
    if (Input::exists('login')) {
        #This is also stupid, rather use config get class
        if (Input::get('username') == ADMIN_USERNAME && Input::get('password') == ADMIN_PASSWORD )  {
           Session::put('username', ADMIN_USERNAME); #temp holder
           Redirect::to('admin.php')
        } else {
            $results['errorMessage'] = "Incorrect username or password";
            require(TEMPLATE_PATH. ''); #Set to Config get login form
        }
        
    }
}

function logout()
{
    Session::delete('username');
    Redirect::to('login.php');
}

function newArticle()
{
    $results = array();
    $results['pageTitle'] = "New Article";
    $results['formAction'] = "newArticle";

    if (Input::exists('saveChanges')) {
        $article = new Article;
        $article->storeFormValues($_POST);
        $article->insert();
        Redirect::to('admin.php?status=changesSaved');
    } elseif (Input::exists('cancel')) {
        Redirect::to('admin.php?action=listArticles');
    } else {
        $results['article'] = new Article;
        require(TEMPLATE_PATH . "/admin/editArticle.php") #Use Config Edit Article
    }
}

function editArticle()
{
    $results = array();
    $results['pageTitle'] = "Edit Article";
    $results['formAction'] = "editArticle";
    if (Input::exists('saveChanges')) {
        if (!$article = Article::getById((int) Input::get('articleId'))) {
            Redirect::to('admin.php?error=articleNotFound');
            return;
        }
        $article->storeFormValues($_POST);
        $article->update();
        Redirect::to('admin.php?status=changesSaved');
    } elseif (Input::exists('cancel')) {
        Redirect::to('admin.php');
    } else {
        $results['article'] = Article::getById((int) Input::get('articleId'));
        require(TEMPLATE_PATH . "/admin/editArticle.php");
    }
}

function deleteArticle()
{
    if (!$article = Article::getById((int) Input::get('articleId'))) {
        Redirect::to('admin.php?error=articleNotFound');
        return;
    }
    $article->delete();
    Redirect::to('admin.php?status=articleDeleted');
}

function listArticles()
{
    $results = array();
    $data = Article::getList();
    $results['articles'] = $data
}
?>