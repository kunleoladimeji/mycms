<?php
//safe_mode_exec_dir=Off note later
ini_set("display_errors", true);
date_default_timezone_set("Indian/Mauritius");
// define('DB_DSN', 'mysql:host=localhost;dbname=cms');
// define('DB_USERNAME', 'username');
// define('DB_PASSWORD', 'password');
// define('CLASS_PATH', 'classes');
// define('TEMPLATE_PATH', 'templates');
// define('HOMEPAGE_NUM_ARTICLES', '5');
// define('ADMIN_USERNAME', 'admin');
// define('ADMIN_PASSWORD', 'admin_pass');
// define('CURRENT_THEME', 'themes/simple');

$GLOBALS['config'] = array(
    'mysql' => array(
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'db' => 'cms',
    'db_type' => 'mysql'
    ),
    'site' => array(
    'class_path' => 'core/classes/',
    'template_path' => 'themes/',
    'admin_username' => 'admin',
    'admin_password' => 'admin_pass',
    'current_theme' => 'themes/simple/',
    'articles_list' => '10'
    )
);

spl_autoload_register(function ($class) {
    require_once 'core/classes/' .$class. '.php';
});
require 'core/classes/AltoRouter.php';

$routes = new AltoRouter();
//$routes->addMatchTypes(array('sBox' => '[^\?s=$][0-9A-Za-z]++'));
$routes->addMatchTypes(array('sBox' => '[\?]'));
#All of these routes will be put into a router class
$routes->addRoutes(array(
    array('GET', '/', function () {
        include 'themes/simple/index.php';
    }, 'home'),
    array('GET', '/home/', function () {
        include 'themes/simple/index.php';
    }, 'home2'),
    array('GET', '/contact/', function () {
        include 'themes/simple/contact.php';
    }, 'contact'),
    array('GET', '/blogs/', function () {
        $article = new Article();
        $result['articles'] = $article->getList('articles');
        $article::article_functions();
        include 'themes/simple/blogs-list.php';
    }, 'blogs'),
    array('GET', '/projects/', function () {
        include 'themes/simple/projects.php';
    }, 'project-list'),
    array('GET', '/projects/[i:id]/', function ($projID) {
        include 'themes/simple/single-project.php';
    }, 'single-project'),
    array('GET', '/blogs/new/', function () {
        include 'themes/simple/new-post.php';
    }, 'new-blog'),
    array('POST', '/blogs/new/', function () {
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'title' => array('required' => true),
            'date' => array('required' => true)
        ));
        $article = new Article();
        $articleData = array(
            'author' => 'Olakunle Oladimeji',
            'title' => Input::get('title'),
            'publicationDate' => Input::get('date'),
            'content' => Input::get('content'),
            'summary' => $article->trim_content(Input::get('content'))
        );
        #Here will put them through validation
        
        $article->storeFormValues($articleData);
        if ($article->insert()) {
            $result = "Inserted and ID is" .$article->id;
        } else {
            $result = $article->error;
        }
        include 'themes/simple/new-post.php';
    }, 'post-blog'),
    array('GET', '/search/', function () {
        #AltoRouter does not accept query string params
        include 'themes/simple/search.php';
    }, 'search'),
    array('GET', '/photos/', function () {
        include 'themes/simple/photos.php';
    }, 'photostream'),
    array('GET', '/facebook/', function () {
        Redirect::to('http://www.facebook.com/kunle.oladimeji');
    }, 'facebook'),
    array('GET', '/twitter/', function () {
        Redirect::to('http://www.twitter.com/IamOlakunle');
    }, 'twitter'),
    array('GET', '/gplus/', function () {
        Redirect::to('https://plus.google.com/u/0/108977535277454313351');
    }, 'google-plus'),
    array('GET', '/bitbucket/', function () {
        Redirect::to('https://bitbucket.org/kunleoladimeji/');
    }, 'bitbucket'),
    array('GET', '/skype/', function () {
        Redirect::to('skype:olakunle.oladimeji?chat');
    }, 'skype'),
));

function handleException($exception)
{
    //echo "Sorry, a problem occured. Please try later.";
    echo $exception;
    error_log($exception->getMessage());
}

function site_header()
{
    include 'themes/simple/header.php';
}

function is_active()
{
}

function site_footer()
{
    include 'themes/simple/footer.php';
}

function escape($string) {
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

set_exception_handler('handleException'); #Add try-catch blocks when you have time
if (!function_exists('base_url')) {
    function base_url($atRoot=false, $atCore=false, $parse=false)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
            
            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), null, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];
            
            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else {
            $base_url = 'http://localhost/';
        }
            
        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) {
                if ($base_url['path'] == '/') {
                    $base_url['path'] = '';
                }
            }
        }
        
        return $base_url;
    }
}
